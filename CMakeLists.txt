########################################################################
#
#       Section générale
#
########################################################################

# La version de cmake minimum
cmake_minimum_required(VERSION 2.6)

# Le nom du projet
project(BatailleSpatiale)

# Enregistrement des répertoire utiles
set(CLIENT_SOURCE_DIR "${CMAKE_SOURCE_DIR}/src/client")
set(COMMUN_SOURCE_DIR "${CMAKE_SOURCE_DIR}/src/commun")
set(SERVER_SOURCE_DIR "${CMAKE_SOURCE_DIR}/src/serveur")

if(WIN32)
    # Le répertoire où est installée la sfml (windows uniquement)
    set(SFML_DIR "D:/SFML/SFML-2.1")
endif()

# Par défaut on fait une compilation de Debug
if(CMAKE_BUILD_TYPE STREQUAL "")
    set(CMAKE_BUILD_TYPE Debug)
endif()

########################################################################
#
#       Section des sources
#
########################################################################

# Le nom des exécutables
set(EXECUTABLE_CLIENT_NAME "BatailleSpatiale")
set(EXECUTABLE_SERVER_NAME "ServeurBatailleSpatiale")
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_SOURCE_DIR}/bin")

# Récuperer les fichiers sources
file(GLOB_RECURSE SOURCE_CLIENT_FILES "${CLIENT_SOURCE_DIR}/*")
file(GLOB_RECURSE SOURCE_COMMUN_FILES "${COMMUN_SOURCE_DIR}/*")
file(GLOB_RECURSE SOURCE_SERVER_FILES "${SERVER_SOURCE_DIR}/*")

# Ajouter les exécutables
add_executable(${EXECUTABLE_CLIENT_NAME} ${SOURCE_CLIENT_FILES} ${SOURCE_COMMUN_FILES})
add_executable(${EXECUTABLE_SERVER_NAME} ${SOURCE_SERVER_FILES} ${SOURCE_COMMUN_FILES})

########################################################################
#
#       Section configuration générale
#
########################################################################

# Si on compile sous windows
if(WIN32)
    # On ne met pas de prefix d'installation
    set(CMAKE_INSTALL_PREFIX "")
    
    # On défini le répertoire include de la sfml
    set(SFML_INCLUDE_DIR "${SFML_DIR}/include")
    
    # On défini le répertoire racine de la sfml
    set(SFML_ROOT ${SFML_DIR})
    
    # Options windows
    set(CMAKE_CXX_FLAGS "-mwindows")
endif()

# On met les librairies standards
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-g -std=c++0x -Wall -Wextra -Werror")

########################################################################
#
#       Section librairies
#
########################################################################

# On défini le répertoire des modules cmake
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules")

# SFML :
set(SFML_STATIC_LIBRARIES FALSE)
#Cherche une version 2 de la sfml 
#Voir FindSFML.cmake pour plus de détails et d'instructions
find_package(SFML 2 REQUIRED system network)

#Si la sfml est trouvée on l'ajoute
if(SFML_FOUND)
  include_directories(${SFML_INCLUDE_DIR})
  target_link_libraries(${EXECUTABLE_SERVER_NAME} ${SFML_LIBRARIES})
  target_link_libraries(${EXECUTABLE_CLIENT_NAME} ${SFML_LIBRARIES})
endif()

########################################################################
#
#       Section configurations finales
#
########################################################################

# Le package CPack
include(InstallRequiredSystemLibraries)
include(CPack)
