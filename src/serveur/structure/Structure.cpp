#include "Structure.hpp"

// Constructeur
Structure::Structure(sf::Int32 _vieMax, sf::Int32 _bouclierMax, float _bouclierTaux,
                     sf::Int32 _visibilite, sf::Int32 _rayonAttaque, sf::Int32 _attaque) {

    vie = vieMax = _vieMax;
    bouclier = bouclierMax = _bouclierMax;
    bouclierTaux = _bouclierTaux;
    visibilite = _visibilite;
    rayonAttaque = _rayonAttaque;
    attaque = _attaque;
}

sf::Int32 Structure::getVie() const {
    return vie;
}

sf::Int32 Structure::getVieMax() const {
    return vieMax;
}

sf::Int32 Structure::getBouclier() const {
    return bouclier;
}

sf::Int32 Structure::getBouclierMax() const {
    return bouclierMax;
}

float Structure::getBouclierTaux() const {
    return bouclierTaux;
}

sf::Int32 Structure::getVisibilite() const {
    return visibilite;
}

sf::Int32 Structure::getRayonAttaque() const {
    return rayonAttaque;
}

sf::Int32 Structure::getAttaque() const {
    return attaque;
}

void Structure::setVie(sf::Int32 const _vie) {

    if(vie < 0) vie = 0;
    else if(_vie <= vieMax) vie = _vie;
    else vie = vieMax;

}

void Structure::setVieMax(sf::Int32 const _vieMax) {

    vieMax = _vieMax;

}

/*
 *  Cette methode renvoie le parametre passe si celui ci est negatif
 *  pour savoir de combien le bouclier a ete depasse si jamais on recoit
 *  des degats
 */

sf::Int32 Structure::setBouclier(sf::Int32 const _bouclier) {

    if(_bouclier < 0) {
        bouclier = 0;
        return -_bouclier; // On met un - pour avoir une valeur positive
    } else if((_bouclier <= bouclierMax)) {
        bouclier = _bouclier;
    } else bouclier = bouclierMax;

    return 0;
}


void Structure::setBouclierMax(sf::Int32 const _bouclierMax) {
    bouclierMax = _bouclierMax;
}

void Structure::setBouclierTaux(float const _bouclierTaux) {
    bouclierTaux = _bouclierTaux;
}

void Structure::setVisibilite(sf::Int32 const _visibilite) {
    visibilite = _visibilite;
}

void Structure::setRayonAttaque(sf::Int32 const _rayonAttaque) {
    rayonAttaque = _rayonAttaque;
}

void Structure::setAttaque(sf::Int32 const _attaque) {
    attaque = _attaque;
}

/* Cette methode sert a ajouter ou a retirer un certain
 * nombre de posf::Int32s de vie a une structure ciblee    */
void Structure::modifierVie(sf::Int32 const valeur) {
    if(vie + valeur < vieMax) {
        setVie(vie + valeur);
    } else setVie(vieMax);
}

/* Cette methode sert à faire baisser la vie de la
 * structure en utilisant l'attaque de l'attaquant
 * repartie entre la vie et le bouclier.
 * Si le bouclier casse alors ce qu'il n'a pas
 * absorbe est envoye a la vie                  */
void Structure::subir(Structure const& attaquant) {
    /* Le total des degats est compose des degats
    * qui sont attribues a la vie selon le taux
    * d'absorbsion du bouclier + ce que le
    * bouclier n'a pas absorbé                 */
    this->modifierVie( - (attaquant.getAttaque() * (1-bouclierTaux) + ( this->setBouclier( bouclier - (attaquant.getAttaque() * bouclierTaux ) ) ) ) ) ;
}

/* Meme methode en mode ciblage                */
void Structure::attaquer(Structure& cible) const {
    cible.subir(*this);
}

Structure Structure::cloner(Structure const& modele, TechnologieStructure techS) {
    Structure copie(modele.getVieMax() + (modele.getVieMax() * coefficientTechnologieVie * techS.getNiveauVie()),
                    modele.getBouclierMax() + (modele.getBouclierMax() * coefficientTechnologieBouclier * techS.getNiveauBouclier()),
                    modele.getBouclierTaux() + (coefficientTechnologieBouclier * techS.getNiveauBouclier()),
                    modele.getVisibilite() + (modele.getVisibilite() * coefficientTechnologieVisibilite * techS.getNiveauVisibilite()),
                    modele.getRayonAttaque() + (modele.getRayonAttaque() * coefficientTechnologieRayonAttaque * techS.getNiveauRayonAttaque()),
                    modele.getAttaque() + (modele.getAttaque() * coefficientTechnologieAttaque * techS.getNiveauAttaque()));

    return copie;

}

sf::Packet& operator <<(sf::Packet& paquet, const Structure& structure) {
    return paquet << structure.getVie() << structure.getVieMax() << structure.getBouclier()
           << structure.getBouclierMax()  << structure.getBouclierTaux()
           << structure.getVisibilite() << structure.getRayonAttaque()
           << structure.getAttaque();
}
