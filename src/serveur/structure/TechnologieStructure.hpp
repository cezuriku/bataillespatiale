#ifndef TECHNOLOGIE_STRUCTURE_HPP
#define TECHNOLOGIE_STRUCTURE_HPP
#include <SFML/System.hpp>
#include <SFML/Network.hpp>

/**
 * Classe qui définit les technologies sur les structures
 * que possède le joueur.
 * Elle est utile lorsque l'on crée une nouvelle structure
 */

class TechnologieStructure {

public:

    TechnologieStructure();

    // Getters
    sf::Int16 getNiveauAttaque() const;
    sf::Int16 getNiveauBouclier() const;
    sf::Int16 getNiveauVie() const;
    sf::Int16 getNiveauVisibilite() const;
    sf::Int16 getNiveauRayonAttaque() const;

    // Des méthodes pour augmenter les niveaux
    bool augmenterNiveauAttaque(sf::Int16 const& gainNiveau);
    bool augmenterNiveauBouclier(sf::Int16 const& gainNiveau);
    bool augmenterNiveauVie(sf::Int16 const& gainNiveau);
    bool augmenterNiveauVisibilite(sf::Int16 const& gainNiveau);
    bool augmenterNiveauRayonAttaque(sf::Int16 const& gainNiveau);


private:

    // Cette classe possède différents niveaux utiles pour toutes les structures
    sf::Int16 niveauAttaque;
    sf::Int16 niveauBouclier;
    sf::Int16 niveauVie;
    sf::Int16 niveauVisibilite;
    sf::Int16 niveauRayonAttaque;
};

sf::Packet& operator <<(sf::Packet& paquet, const TechnologieStructure& technologie);

#endif // TECHNOLOGIE_STRUCTURE_HPP
