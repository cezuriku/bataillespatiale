#ifndef STRUCTURE_HPP
#define STRUCTURE_HPP
#include "TechnologieStructure.hpp"
#include <memory>
#include <SFML/System.hpp>

/*
 * Classe qui définit les differents attributs
 * que possèdent les batiments et les vaisseaux.
 * Les classes Vaisseau et Batiment en héritent
 */

class Structure {

public:
    Structure(sf::Int32 _vieMax = 100, sf::Int32 _bouclierMax = 0, float _bouclierTaux = 0, sf::Int32 _visibilite = 0, sf::Int32 _rayonAttaque = 0, sf::Int32 _attaque = 10);

    // Getter
    sf::Int32 getVie() const;
    sf::Int32 getVieMax() const;
    sf::Int32 getBouclier() const;
    sf::Int32 getBouclierMax() const;
    float getBouclierTaux() const;
    sf::Int32 getVisibilite() const;
    sf::Int32 getRayonAttaque() const;
    sf::Int32 getAttaque() const;

    // Setter
    void setVie( sf::Int32 const _vie);
    sf::Int32  setBouclier(sf::Int32 const _bouclier);
    void setVieMax( sf::Int32 const _vieMax);
    void setBouclierMax( sf::Int32 const _bouclierMax);
    void setBouclierTaux(float const _bouclierTaux);
    void setVisibilite( sf::Int32 const _visibilite);
    void setRayonAttaque( sf::Int32 const _rayonAttaque);
    void setAttaque( sf::Int32 const _attaque);

    void subir(Structure const& attaquant);
    void attaquer(Structure& cible) const;
    void modifierVie(sf::Int32 const valeur);
    static Structure cloner(Structure const& modele, TechnologieStructure techS);


protected:
    sf::Int32 vie;
    sf::Int32 vieMax;
    sf::Int32 bouclier;
    sf::Int32 bouclierMax;
    float bouclierTaux;
    sf::Int32 visibilite;
    sf::Int32 rayonAttaque;
    sf::Int32 attaque;

private:
    static constexpr float coefficientTechnologieAttaque = 0.5f;
    static constexpr float coefficientTechnologieBouclier = 0.5f;
    static constexpr float coefficientTechnologieVie = 0.5f;
    static constexpr float coefficientTechnologieVisibilite = 0.5f;
    static constexpr float coefficientTechnologieRayonAttaque = 0.5f;
};

sf::Packet& operator <<(sf::Packet& paquet, const Structure& structure);

typedef std::shared_ptr<Structure> StructurePtr;

#endif // STRUCTURE_HPP
