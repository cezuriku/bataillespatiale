#ifndef TECHNOLOGIE_VAISSEAU_HPP
#define TECHNOLOGIE_VAISSEAU_HPP
#include <SFML/Network.hpp>

/*
 * Classe qui définit les technologies sur les vaisseaux
 * que possède le joueur.
 * Elle est utile lorsque l'on crée un nouveau vaisseau
 */

class TechnologieVaisseau {

public:
    TechnologieVaisseau();
    // Getters
    sf::Int16 getNiveauMateriaux() const;
    sf::Int16 getNiveauDeplacement() const;
    sf::Int16 getNiveauConsommation() const;
    // Des méthodes pour augmenter les niveaux
    bool augmenterNiveauMateriaux(sf::Int16 const& gainNiveau);
    bool augmenterNiveauDeplacement(sf::Int16 const& gainNiveau);
    bool augmenterNiveauConsommation(sf::Int16 const& gainNiveau);

private:
    // Cette classe possède différents niveaux utiles pour toutes les structures
    sf::Int16 niveauMateriaux;
    sf::Int16 niveauDeplacement;
    sf::Int16 niveauConsommation;

};

sf::Packet& operator <<(sf::Packet& paquet, const TechnologieVaisseau& technologie);

#endif // TECHNOLOGIE_VAISSEAU_HPP
