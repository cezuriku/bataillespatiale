#include "Vaisseau.hpp"

Vaisseau::Vaisseau() : Structure() {
    consommation = 1;
    deplacementMax = 5;
    type = TypeVaisseau::Inexistant;
}

Vaisseau::Vaisseau(sf::Int32 vieMax, sf::Int32 bouclierMax, float bouclierTaux, sf::Int32 visibilite,
                   sf::Int32 attaque, sf::Int32 _consommation, sf::Int32 _deplacementMax, TypeVaisseau _type):
    Structure(vieMax, bouclierMax, bouclierTaux, visibilite, attaque) {

    consommation = _consommation;
    deplacementMax = _deplacementMax;
    type = _type;

}

sf::Int32 Vaisseau::getConsommation() const {
    return consommation;
}

void Vaisseau::setConsommation(sf::Int32 const _consommation) {
    consommation = _consommation;
}

TypeVaisseau Vaisseau::getType() const {
    return type;
}

Vaisseau Vaisseau::cloner(Vaisseau const& modele, TechnologieStructure techS, TechnologieVaisseau techV) {

    Structure base = Structure::cloner(modele, techS);

    return Vaisseau(base.getVieMax(),
                    base.getBouclierMax(),
                    base.getBouclierTaux(),
                    base.getVisibilite(),
                    base.getAttaque(),
                    modele.getConsommation() - (modele.getConsommation() * 0.1 * techV.getNiveauConsommation()),
                    modele.getDeplacementMax(),
                    modele.getType());

}

sf::Int32 Vaisseau::getDeplacementMax() const {
    return deplacementMax;
}

sf::Packet& operator <<(sf::Packet& paquet, const Vaisseau& vaisseau) {
    return paquet << vaisseau.getVie() << vaisseau.getVieMax() << vaisseau.getBouclier()
           << vaisseau.getBouclierMax()  << vaisseau.getBouclierTaux()
           << vaisseau.getVisibilite() << vaisseau.getRayonAttaque()
           << vaisseau.getAttaque() << vaisseau.getConsommation()
           << vaisseau.getDeplacementMax() << static_cast<sf::Int16>(vaisseau.getType());
}
