#include "TechnologieVaisseau.hpp"

/*
 * Cette classe ne contient que les niveaux influençant touts les vaisseaux
 * Le constructeur met tous les niveaux à 0
 */
TechnologieVaisseau::TechnologieVaisseau():
    niveauMateriaux(0), niveauDeplacement(0), niveauConsommation(0) {

}

sf::Int16 TechnologieVaisseau::getNiveauMateriaux() const {
    return niveauMateriaux;
}

sf::Int16 TechnologieVaisseau::getNiveauDeplacement() const {
    return niveauDeplacement;
}

sf::Int16 TechnologieVaisseau::getNiveauConsommation() const {
    return niveauConsommation;
}


bool TechnologieVaisseau::augmenterNiveauMateriaux(sf::Int16 const& gainNiveau) {
    // Le niveau Materiaux ne peut pas augmenter de plus de 10 niveaux à la fois
    if(gainNiveau > 10)
        return false;

    // Si il n'est pas supérieur à 10 on l'augmente de [gainNiveau]
    niveauMateriaux += gainNiveau;
    return true;
}

bool TechnologieVaisseau::augmenterNiveauDeplacement(sf::Int16 const& gainNiveau) {
    // Le niveau Deplacement ne peut pas augmenter de plus de 5 niveaux à la fois
    if(gainNiveau > 5)
        return false;

    // Si il n'est pas supérieur à 5 on l'augmente de [gainNiveau]
    niveauDeplacement += gainNiveau;
    return true;
}

bool TechnologieVaisseau::augmenterNiveauConsommation(sf::Int16 const& gainNiveau) {
    // Le niveau Consommation ne peut pas augmenter de plus de 3 niveaux en même temps
    if(gainNiveau > 3)
        return false;

    // Si il n'est pas supérieur à 3 on l'augmente de [gainNiveau]
    niveauConsommation += gainNiveau;
    return true;
}

sf::Packet& operator <<(sf::Packet& paquet, const TechnologieVaisseau& technologie) {
    return paquet << technologie.getNiveauConsommation() << technologie.getNiveauDeplacement()
           << technologie.getNiveauMateriaux();
}
