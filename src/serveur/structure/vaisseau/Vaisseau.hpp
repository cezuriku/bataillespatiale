#ifndef VAISSEAU_HPP
#define VAISSEAU_HPP
#include "../Structure.hpp"
#include "TechnologieVaisseau.hpp"
#include "../../../commun/TypeVaisseau.hpp"
#include <memory>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>

/*
 * Classe qui définit les differents attributs
 * que possèdent les vaisseaux.
 */
class Vaisseau : public Structure {

public:
    Vaisseau();
    Vaisseau(sf::Int32 vieMax,
             sf::Int32 bouclierMax,
             float bouclierTaux,
             sf::Int32 visibilite,
             sf::Int32 attaque,
             sf::Int32 _consommation,
             sf::Int32 _deplacementMax,
             TypeVaisseau _type = TypeVaisseau::Inexistant);

    // Getters
    sf::Int32 getConsommation() const;
    sf::Int32 getDeplacementMax() const;
    TypeVaisseau getType() const;

    // Setters
    void setConsommation(sf::Int32 const consommation);

    static Vaisseau cloner(Vaisseau const& modele, TechnologieStructure techS, TechnologieVaisseau techV);

protected:
    TypeVaisseau type;

private:
    sf::Int32 deplacementMax;
    sf::Int32 consommation;

};

sf::Packet& operator <<(sf::Packet& paquet, const Vaisseau& vaisseau);

typedef std::shared_ptr<Vaisseau> VaisseauPtr;


#endif // VAISSEAU_HPP
