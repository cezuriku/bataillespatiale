#ifndef BATIMENT_HPP
#define BATIMENT_HPP
#include <memory>
#include <SFML/System.hpp>
#include "../Structure.hpp"
#include "../../../commun/TypeBatiment.hpp"

/*
 * Classe qui définit les differents attributs
 * que possèdent les batiments.
 */
 
class Batiment : public Structure {

    public:
		Batiment();
		Batiment(sf::Int32 vieMax, sf::Int32 bouclierMax, float bouclierTaux, sf::Int32 visibilite, sf::Int32 attaque, sf::Int32 niveau, TypeBatiment _type = TypeBatiment::Inexistant);
		TypeBatiment getType() const;
		sf::Int32 getNiveau() const;
		void setNiveau(sf::Int32 niveau);

	protected:
		TypeBatiment type;
        
    private:
		sf::Int32 niveau;
};

sf::Packet& operator <<(sf::Packet& paquet, const Batiment& batiment);

typedef std::shared_ptr<Batiment> BatimentPtr;

#endif // BATIMENT_HPP


