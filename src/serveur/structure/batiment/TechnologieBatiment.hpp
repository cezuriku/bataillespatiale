#ifndef TECHNOLOGIE_BATIMENT_HPP
#define TECHNOLOGIE_BATIMENT_HPP
#include <iostream>
#include <SFML/System.hpp>

/*
 * Classe qui définit les technologies sur les vaisseaux
 * que possède le joueur.
 * Elle est utile lorsque l'on crée un nouveau vaisseau
 */

class TechnologieBatiment {

	public:

		TechnologieBatiment();
		// Getters
		sf::Int16 getNiveauAchat() const;
		sf::Int16 getNiveauCoutEnergie() const;
		sf::Int16 getNiveauCoutMateriaux() const;
		// Des méthodes pour augmenter les niveaux
		bool augmenterNiveauAchat(sf::Int16 const& gainNiveau);
		bool augmenterNiveauCoutEnergie(sf::Int16 const& gainNiveau);
		bool augmenterNiveauCoutMateriaux(sf::Int16 const& gainNiveau);


	private:

		// Cette classe possède différents niveaux utiles pour toutes les structures
		sf::Int16 niveauAchat;
		sf::Int16 niveauCoutEnergie;
		sf::Int16 niveauCoutMateriaux;

		// Deux méthodes pour afficher notre structure
		void changerFlux(std::ostream &fluxSortant) const;
		friend std::ostream& operator<< (std::ostream& fluxSortant, TechnologieBatiment const& technologieBatiment);
};


#endif // TECHNOLOGIE_BATIMENT_HPP


