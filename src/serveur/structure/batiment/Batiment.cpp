#include "Batiment.hpp"
#include "../../../commun/TypeBatiment.hpp"

Batiment::Batiment() : Structure() {
    niveau = 0;
}

Batiment::Batiment(sf::Int32 vieMax, sf::Int32 bouclierMax, float bouclierTaux, sf::Int32 visibilite, sf::Int32 attaque, sf::Int32 niveau, TypeBatiment _type) :
    Structure(vieMax, bouclierMax, bouclierTaux, visibilite, attaque) {
    this->niveau = niveau;
    type = _type;
}

sf::Int32 Batiment::getNiveau() const {
    return niveau;
}

void Batiment::setNiveau(sf::Int32 niveau) {
    this->niveau = niveau;
}

TypeBatiment Batiment::getType() const {
    return type;
}

sf::Packet& operator <<(sf::Packet& paquet, const Batiment& batiment) {
    return paquet << batiment.getVie() << batiment.getVieMax() << batiment.getBouclier()
           << batiment.getBouclierMax()  << batiment.getBouclierTaux()
           << batiment.getVisibilite() << batiment.getRayonAttaque()
           << batiment.getAttaque() << batiment.getNiveau() << static_cast<sf::Int16>(batiment.getType());
}
