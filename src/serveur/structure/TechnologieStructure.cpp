#include "TechnologieStructure.hpp"

TechnologieStructure::TechnologieStructure():
    niveauAttaque(0), niveauBouclier(0), niveauVie(0), niveauVisibilite(0) {

}

sf::Int16 TechnologieStructure::getNiveauAttaque() const {
    return niveauAttaque;
}

sf::Int16 TechnologieStructure::getNiveauBouclier() const {
    return niveauBouclier;
}

sf::Int16 TechnologieStructure::getNiveauVie() const {
    return niveauVie;
}

sf::Int16 TechnologieStructure::getNiveauVisibilite() const {
    return niveauVisibilite;
}

sf::Int16 TechnologieStructure::getNiveauRayonAttaque() const {
    return niveauRayonAttaque;
}

bool TechnologieStructure::augmenterNiveauAttaque(sf::Int16 const& gainNiveau) {
    // Le niveau Attaque ne peut pas augmenter de plus de 10 niveaux à la fois
    if(gainNiveau > 10)
        return false;

    // Si il n'est pas supérieur à 10 on l'augmente de [gainNiveau]
    niveauAttaque += gainNiveau;
    return true;
}

bool TechnologieStructure::augmenterNiveauBouclier(sf::Int16 const& gainNiveau) {
    // Le niveau Bouclier ne peut pas augmenter de plus de 5 niveaux à la fois
    if(gainNiveau > 5)
        return false;

    // Si il n'est pas supérieur à 5 on l'augmente de [gainNiveau]
    niveauBouclier += gainNiveau;
    return true;
}

bool TechnologieStructure::augmenterNiveauVie(sf::Int16 const& gainNiveau) {
    // Le niveau Vie ne peut pas augmenter de plus de 3 niveaux en même temps
    if(gainNiveau > 3)
        return false;

    // Si il n'est pas supérieur à 3 on l'augmente de [gainNiveau]
    niveauVie += gainNiveau;
    return true;
}

bool TechnologieStructure::augmenterNiveauVisibilite(sf::Int16 const& gainNiveau) {
    // Le niveau Visibilité ne peut pas augmenter de plus de 2 niveaux à la fois
    if(gainNiveau > 2)
        return false;

    // Si il n'est pas supérieur à 2 on l'augmente de [gainNiveau]
    niveauVisibilite += gainNiveau;
    return true;
}

bool TechnologieStructure::augmenterNiveauRayonAttaque(sf::Int16 const& gainNiveau) {
    // Le niveau RayonAttaque ne peut pas augmenter de plus de 2 niveaux à la fois
    if(gainNiveau > 2)
        return false;

    // Si il n'est pas supérieur à 2 on l'augmente de [gainNiveau]
    niveauRayonAttaque += gainNiveau;
    return true;
}

sf::Packet& operator <<(sf::Packet& paquet, const TechnologieStructure& technologie) {
    return paquet << technologie.getNiveauAttaque() << technologie.getNiveauBouclier()
           << technologie.getNiveauVie() << technologie.getNiveauVisibilite()
           << technologie.getNiveauRayonAttaque();
}
