#include <iostream>
#include "Serveur.hpp"
#include "../commun/TypePaquet.hpp"

Serveur::Serveur(int _port) :
    port(_port), dimensionPlateauX(10), dimensionPlateauY(10),
    maxJoueur(50) {

}

Serveur::~Serveur() {
    for(std::list<sf::TcpSocket*>::const_iterator socket(listeSocket.begin());
            socket != listeSocket.end(); ++socket) {
                (*socket)->disconnect();
            }
            
    for(std::list<sf::Thread*>::const_iterator thread(listeThread.begin());
            thread != listeThread.end(); ++thread) {
                (*thread)->terminate();
            }
}

void Serveur::envoyerMessage(std::string message) {
    sf::Packet paquet;
    paquet << static_cast<sf::Uint16>(TypePaquet::MessageEchoServeur) << message;
    envoyerPaquetPourTous(paquet);
}

void Serveur::setMaxJoueur(const sf::Int16& _maxJoueur) {
    maxJoueur = _maxJoueur;
}

void Serveur::setDimensionsPlateau(sf::Int32 x, sf::Int32 y) {
    dimensionPlateauX = x;
    dimensionPlateauY = y;
}

int Serveur::getPort() {
    return port;
}

void Serveur::setPort(int _port) {
    port =_port;
}

void Serveur::lancer() {
    plateau = new Plateau(dimensionPlateauX, dimensionPlateauY);
    plateau->setMaxJoueur(maxJoueur);
    if (tcpEcouteur.listen(port) != sf::Socket::Done)
        return;

    //On créé un thread pour attendre la connexion des clients
    listeThread.push_back(new sf::Thread(&Serveur::attendreConnexion, this));
    listeThread.back()->launch();
}

void Serveur::attendreConnexion() {
    while(true) {
        //On ajoute un socket a la liste
        listeSocket.push_back(new sf::TcpSocket);
        //Des qu'un client se connecte
        if (tcpEcouteur.accept(*listeSocket.back()) != sf::Socket::Done)
            return;

        //On créé un thread et on gere ce client avec la derniere socket enregistré
        listeThread.push_back(new sf::Thread(std::bind(&Serveur::gererClient, this, listeSocket.back())));
        //On lance le thread
        listeThread.back()->launch();
    }
}

void Serveur::demarrerPartie() {
    
}

void Serveur::gererClient(sf::TcpSocket *_socket) {
    std::cout << "Nouveau thread" << std::endl;
    
    sf::TcpSocket *socket = _socket;
    sf::Socket::Status codeRetour;
    sf::Packet paquet;
    
    sf::Int32 idClient;
    
    idClient = plateau->ajouterJoueur();
    
    paquet << static_cast<sf::Uint16>(TypePaquet::NumeroJoueur) << idClient;
    paquet << static_cast<sf::Uint16>(TypePaquet::Plateau) << *plateau;
    
    socket->send(paquet);
    paquet.clear();
    
    sf::Packet paquet2;
    
    paquet2 << static_cast<sf::Uint16>(TypePaquet::Joueur) << plateau->getJoueur(idClient);
    
    envoyerPaquetPourTous(paquet2);
    
    if(idClient == maxJoueur -1)
        demarrerPartie();
    
    while(codeRetour == sf::Socket::Done) {
        if ((codeRetour = socket->receive(paquet)) == sf::Socket::Done) {
            while(!paquet.endOfPacket()) {
                // On traite ici le paquet
                sf::Uint16 typePaquet = static_cast<sf::Uint16>(TypePaquet::Vide);
                paquet >> typePaquet;
                switch(static_cast<TypePaquet>(typePaquet)) {
                case TypePaquet::MessageEchoServeur: {
                    std::string message;
                    paquet >> message;
                    std::cout << "[" << socket->getRemoteAddress() << "] " << message << std::endl;
                }
                break;
                
                case TypePaquet::MessageEcho: {
                    std::string message;
                    paquet >> message;
                    std::cout << plateau->getJoueur(idClient).getNom() << " dit : " << message;
                    std::cout << std::endl;
                    sf::Packet paquet2;
                    paquet2 << static_cast<sf::Uint16>(TypePaquet::MessageEcho);
                    paquet2 << idClient << message;
                    envoyerPaquetPourTous(paquet2);
                }
                break;

                case TypePaquet::PING: {
                    sf::Packet paquet2;
                    paquet2 << static_cast<sf::Uint16>(TypePaquet::PONG);
                    std::cout << "[" << socket->getRemoteAddress() << "] " << "PING" << std::endl;
                    socket->send(paquet2);
                }
                break;

                case TypePaquet::ChangementDeNom: {
                    std::string nouveauNom;
                    paquet >> nouveauNom;
                    sf::Packet paquet2;
                    paquet2 << static_cast<sf::Uint16>(TypePaquet::ActualiserJoueur);
                    plateau->renommerJoueur(idClient, nouveauNom);
                    paquet2 << idClient << plateau->getJoueur(idClient);
                    envoyerPaquetPourTous(paquet2);
                }
                break;

                default:
                    std::cout << "[ERREUR] Paquet de type : " << typePaquet
                            << " non géré par le serveur" << std::endl;
                    paquet.clear();
                    break;
                }
            }
        }
    }
    
    std::cout << "Thread détruit" << std::endl;
}

void Serveur::envoyerPaquetPourTous(sf::Packet paquet) {
    for(std::list<sf::TcpSocket*>::const_iterator socket(listeSocket.begin());
            socket != listeSocket.end(); ++socket) {
                (*socket)->send(paquet);
            }
}

std::string Serveur::getLocalHost() {
    return sf::IpAddress::LocalHost.toString();
}

std::string Serveur::getAdresseLocale() {
    return sf::IpAddress::getLocalAddress().toString();
}

std::string Serveur::getAdressePublique() {
    return sf::IpAddress::getPublicAddress().toString();
}

void Serveur::attendreFin() {
    listeThread.front()->wait();
}
