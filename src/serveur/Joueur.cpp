#include "Joueur.hpp"

Joueur::Joueur(sf::Int16 _id) :
    id(_id) {
    energie = 100;
    materiaux = 100;
    nom = "";
}

void Joueur::setNom(std::string _nom) {
    nom = _nom;
}

const std::string& Joueur::getNom() const {
    return nom;
}

const sf::Int16& Joueur::getId() const {
    return id;
}

sf::Packet& operator <<(sf::Packet& paquet, const Joueur& joueur) {
    return paquet << joueur.id << joueur.nom << joueur.energie << joueur.materiaux;
}
