#ifndef SERVEUR_HPP
#define SERVEUR_HPP
#include <list>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include "plateau/Plateau.hpp"

/**
 * \brief Créé un serveur pour le jeu BatailleSpatiale
 * Attention pour le lancer penser a utiliser la méthode Serveur::lancer();
 */

class Serveur {
public:
    /**
     * \brief Constructeur par défaut
     * il prend en paramêtre le port sur lequel il se lancera
     */
    Serveur(int _port = 50001);
    /**
     * \brief Destructeur
     * Deconnecte toutes les sockets
     */
    ~Serveur();
    /**
     * \brief Change les futures dimensions du plateau
     *
     * \param x Taille du futur plateau en X
     * \param y Taille du futur plateau en Y
     */
    void setDimensionsPlateau(sf::Int32 x, sf::Int32 y);
    /**
     * \brief Setter maxJoueur
     * 
     * \param _maxJoueur Le nombre maximum de joueur
     */
    void setMaxJoueur(const sf::Int16& _maxJoueur);
    /**
     * \brief Change le port
     *
     * \param _port Le nouveau port
     */
    void setPort(int _port);
    /**
     * \brief Lance le serveur
     */
    void lancer();
    /**
     * \brief Retourne le port
     * 
     * \return port
     */
    int getPort();
    /**
     * \brief Retourne l'adresse IP localhost
     * 
     * \return localhost
     */
    static std::string getLocalHost();
    /**
     * \brief Retourne l'adresse IP locale
     * 
     * \return adresse IP locale
     */
    std::string getAdresseLocale();
    /**
     * \brief Retourne l'adresse IP publique
     * 
     * \return adresse IP publique
     */
    std::string getAdressePublique();
    /**
     * \brief Permet d'attendre que le thread serveur se termine
     * 
     */
    void attendreFin();
    /**
     * \brief envoie un message à tout les clients
     * 
     * \param message Le message à envoyer
     */
    void envoyerMessage(std::string message);
    
private:
    /**
     * \brief Gere les paquets envoyé par un client
     * \param _socket la socket à gérer
     */
    void gererClient(sf::TcpSocket *_socket);
    /**
     * \brief Attend les connexions des clients
     */
    void attendreConnexion();
    /**
     * \brief envoie un paquet a tout les clients
     * 
     * \param le paquet a envoyer
     */
    void envoyerPaquetPourTous(sf::Packet paquet);
    /**
     * \brief Demarre la partie
     */
     void demarrerPartie();
    
    int port;///< Le port sur lequel le serveur se lance
    sf::Int32 dimensionPlateauX;///< Taille du futur plateau en X
    sf::Int32 dimensionPlateauY;///< Taille du futur plateau en Y
    sf::Int16 maxJoueur;///< Nombre de joueurs maximum
    Plateau *plateau;///< Le plateau sur lequel joue les joueurs
    
    /// Liste des threads qui s'éxécuteront en parallèle pour chaque client connécté
    std::list<sf::Thread*> listeThread;
    /// Liste des sockets auquels se connecte les clients
    std::list<sf::TcpSocket*> listeSocket;
    /// L'écouteur tcp il récupere les connexions des clients
    sf::TcpListener tcpEcouteur;
    /// L'adresse locale du serveur
    sf::IpAddress adresseLocale;
    /// L'adresse publique du serveur
    sf::IpAddress adressePublique;
    
};

#endif // SERVEUR_HPP
