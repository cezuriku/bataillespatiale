#include <iostream>
#include "Serveur.hpp"

int main() {

    Serveur serveur;

    serveur.lancer();

    //On affiche les infos du serveur
    std::cout << "Le serveur est en ecoute sur le port " << serveur.getPort() << std::endl;
    std::cout << "Adresse localhost : " << serveur.getLocalHost() << std::endl;
    std::cout << "Adresse sur le reseau local : " << serveur.getAdresseLocale() << std::endl;
    std::cout << "Adresse publique : " << serveur.getAdressePublique() << std::endl;

    const std::string quitter = "exit";

    std::string message = "";

    while(message != quitter) {
        std::cout << "Entrez le message à envoyer \"exit\" pour quitter" << std::endl;
        std::getline(std::cin,message);

        if(message != quitter)
            serveur.envoyerMessage(message);
    }

    return EXIT_SUCCESS;
}
