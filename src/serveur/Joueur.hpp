#ifndef JOUEUR_HPP
#define JOUEUR_HPP
#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <string>

class Joueur {
public:
    Joueur(sf::Int16 _id);
    const sf::Int16& getId() const;
    void setNom(std::string _nom);
    const std::string& getNom() const;

private:
    sf::Int16 id;
    std::string nom;
    sf::Int32 energie;
    sf::Int32 materiaux;
    friend sf::Packet& operator <<(sf::Packet& paquet, const Joueur& joueur);
    
};

static Joueur JoueurNull(-2);

sf::Packet& operator <<(sf::Packet& paquet, const Joueur& joueur);

#endif // JOUEUR_HPP
