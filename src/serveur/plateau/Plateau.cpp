#include "Plateau.hpp"

extern Joueur JoueurNull;

Plateau::Plateau(sf::Int32 _tailleX, sf::Int32 _tailleY) :
    tailleX(_tailleX), tailleY(_tailleY) {

    cellule.resize(tailleX, std::vector<Cellule>(tailleY));

    for (sf::Int32 x = 0 ; x < tailleX ; ++x)
        for (sf::Int32 y = 0 ; y < tailleY ; ++y)
            cellule[x][y] = Cellule();
}

void Plateau::setMaxJoueur(const sf::Int16& _maxJoueur) {
    maxJoueur = _maxJoueur;
}

const sf::Int16& Plateau::getMaxJoueur() const {
    return maxJoueur;
}

sf::Int32 Plateau::ajouterJoueur() {
    sf::Int32 idJoueur;

    mutexListeJoueur.lock();

    if((sf::Int16) listeJoueur.size() >= maxJoueur)
        idJoueur = -1;
    else {
        idJoueur = listeJoueur.size();
        listeJoueur.push_back(Joueur(idJoueur));
    }

    mutexListeJoueur.unlock();

    return idJoueur;
}

sf::Packet& operator <<(sf::Packet& paquet, Plateau& plateau) {
    paquet << plateau.tailleX << plateau.tailleY;

    for (sf::Int32 x = 0 ; x < plateau.tailleX ; ++x)
        for (sf::Int32 y = 0 ; y < plateau.tailleY ; ++y)
            paquet << plateau.cellule[x][y];

    plateau.mutexListeJoueur.lock();

    paquet << (sf::Int16) plateau.listeJoueur.size();

    for(std::list<Joueur>::const_iterator joueur(plateau.listeJoueur.begin());
            joueur != plateau.listeJoueur.end(); ++joueur)
        paquet << *joueur;

    plateau.mutexListeJoueur.unlock();

    return paquet;
}

void Plateau::renommerJoueur(sf::Int16 id, std::string nom) {
    for(std::list<Joueur>::iterator joueur(listeJoueur.begin());
            joueur != listeJoueur.end(); ++joueur) {
        if(joueur->getId() == id)
            joueur->setNom(nom);
    }
}

const Joueur& Plateau::getJoueur(sf::Int16 id) {
    for(std::list<Joueur>::iterator joueur(listeJoueur.begin());
            joueur != listeJoueur.end(); ++joueur) {
        if(joueur->getId() == id)
            return *joueur;
    }
    return JoueurNull;
}
