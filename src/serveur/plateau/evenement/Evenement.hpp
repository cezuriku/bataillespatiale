#ifndef EVENEMENT_HPP
#define EVENEMENT_HPP
#include <memory>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include "../../../commun/TypeEvenement.hpp"
#include "../../structure/batiment/Batiment.hpp"
#include "../../structure/vaisseau/Vaisseau.hpp"

/*
 * Classe qui permet d'affecter les vaisseaux
 * selon certains paramêtres
 */

class Evenement {

public:
    Evenement(TypeEvenement typeEvenement, sf::Int32 _vie = -1, sf::Int32 _coutDeplacement = 1, sf::Int32 _multiplicateurAttaque = 1, sf::Int32 _multiplicateurDegat = 1);

    //Setters
    virtual void activer(__attribute__((unused)) Batiment* batiment) {}
    virtual void desactiver(__attribute__((unused)) Batiment* batiment) {}
    virtual void activer(__attribute__((unused)) Vaisseau* vaisseau) {}
    virtual void desactiver(__attribute__((unused)) Vaisseau* vaisseau) {}

    //Getters
    TypeEvenement getType() const;
    bool estDestructible() const;
    bool estAccessible() const;
    bool tirPossible() const;
    bool ciblePossible() const;
    sf::Int32 getVie() const;
    sf::Int32 getCoutDeplacement() const;
    sf::Int32 getMultiplicateurAttaque() const;
    sf::Int32 getMultiplicateurDegat() const;

private:
    // Nature de l'évènements.
    TypeEvenement typeEvenement;
    // vie de l'obstacle -1 si il n'as pas de vie
    sf::Int32 vie;
    // nombre de deplacement a utiliser pour passer par cette case 1 par défaut -1 pour impossible
    sf::Int32 coutDeplacement;
    // paramètre à prendre en compte lors du calcul des dommages infligés par la structure présente.
    sf::Int32 multiplicateurAttaque;
    // paramètre à prendre en compte lors du calcul des dommages subits par la structure présente.
    sf::Int32 multiplicateurDegat;

};

sf::Packet& operator <<(sf::Packet& paquet, const Evenement& evenement);

typedef std::shared_ptr<Evenement> EvenementPtr;

const Evenement EvenementNuageGaz = Evenement(
        TypeEvenement::NuageGaz,
        -1,
        1,
        2,
        1
    );

const Evenement EvenementChampMeteor = Evenement(
        TypeEvenement::ChampMeteor,
        50,
        -1,
        1,
        1
    );

const Evenement EvenementEpave = Evenement(
         TypeEvenement::Epave,
         50,
         -1,
         1,
         1
     );

const Evenement EvenementInfluenceTrouNoir = Evenement(
        TypeEvenement::InfluenceTrouNoir,
        -1,
        3,
        2,
        2
    );

const Evenement EvenementStationSpatialeAbandonnee = Evenement(
        TypeEvenement::StationSpatialeAbandonnee,
        50,
        -1,
        1,
        1
    );

#endif // EVENEMENT_HPP
