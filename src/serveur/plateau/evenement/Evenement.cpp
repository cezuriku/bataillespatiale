#include <iostream>
#include "Evenement.hpp"

Evenement::Evenement(TypeEvenement _typeEvenement, sf::Int32 _vie, sf::Int32 _coutDeplacement, sf::Int32 _multiplicateurAttaque, sf::Int32 _multiplicateurDegat) {
    vie = _vie;
    coutDeplacement = _coutDeplacement;
    multiplicateurAttaque = _multiplicateurAttaque;
    multiplicateurDegat = _multiplicateurDegat;
    typeEvenement = _typeEvenement;
}

TypeEvenement Evenement::getType() const {
    return typeEvenement;
}

bool Evenement::estDestructible() const {
    return vie > 0;
}

bool Evenement::estAccessible() const {
    return coutDeplacement != -1;
}

bool Evenement::tirPossible() const {
    return multiplicateurAttaque != 0;
}

bool Evenement::ciblePossible() const {
    return multiplicateurDegat != 0;
}

sf::Int32 Evenement::getVie() const {
    return vie;
}

sf::Int32 Evenement::getCoutDeplacement() const {
    return coutDeplacement;
}

sf::Int32 Evenement::getMultiplicateurAttaque() const {
    return multiplicateurAttaque;
}

sf::Int32 Evenement::getMultiplicateurDegat() const {
    return multiplicateurDegat;
}

sf::Packet& operator <<(sf::Packet& paquet, const Evenement& evenement) {
    return paquet << evenement.getVie() << evenement.getCoutDeplacement()
           << evenement.getMultiplicateurAttaque() << evenement.getMultiplicateurDegat()
           << static_cast<sf::Int16>(evenement.getType());
}
