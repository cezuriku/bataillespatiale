#ifndef PLATEAU_HPP
#define PLATEAU_HPP
#include <vector>
#include <list>
#include "Cellule.hpp"
#include "../Joueur.hpp"
#include <SFML/Network.hpp>
#include <SFML/System.hpp>

/**
 * \brief Le plateau coté serveur
 *
 */
class Plateau {

public:
    /**
     * \brief Constructeur du plateau
     *
     * \param _tailleX Taille du plateau en X
     * \param _tailleY Taille du plateau en Y
     */
    Plateau(sf::Int32 _tailleX, sf::Int32 _tailleY);
    /**
     * \brief Setter maxJoueur
     *
     * \param _maxJoueur Le nombre maximum de joueur
     */
    void setMaxJoueur(const sf::Int16& _maxJoueur);
    /**
     * \brief Getter maxJoueur
     *
     * \return maxJoueur
     */
    const sf::Int16& getMaxJoueur() const;
    /**
     * \brief ajoute un joueur
     *
     * \return le numero du joueur -1 si il n'y a plus de place
     */
    sf::Int32 ajouterJoueur();
    /**
     * \brief renomme un joueur
     *
     * \param id l'id du joueur
     * \param nom le nouveau nom du joueur
     */
    void renommerJoueur(sf::Int16 id, std::string nom);
    /**
     * \brief recupere le joueur qui possede cet id
     * 
     * \param id l'id du joueur
     */
    const Joueur& getJoueur(sf::Int16 id);

private:
    /// Les cellules du plateau
    std::vector<std::vector<Cellule>> cellule;
    ///La liste des joueurs présents
    std::list<Joueur> listeJoueur;
    sf::Int32 tailleX;///< Taille du plateau en X
    sf::Int32 tailleY;///< Taille du plateau en Y
    sf::Int16 maxJoueur;///< Nombre de joueurs maximum
    sf::Mutex mutexListeJoueur;///< Mutex pour empecher que 2 threads manipule la liste
    friend sf::Packet& operator <<(sf::Packet& paquet, Plateau& plateau);
};

sf::Packet& operator <<(sf::Packet& paquet, Plateau& plateau);

#endif // PLATEAU_HPP
