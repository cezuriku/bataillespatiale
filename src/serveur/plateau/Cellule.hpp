#ifndef CELLULE_HPP
#define CELLULE_HPP
#include <SFML/Network.hpp>
#include "../../commun/TypeCellule.hpp"
#include "evenement/Evenement.hpp"
#include "../structure/batiment/Batiment.hpp"
#include "../structure/vaisseau/Vaisseau.hpp"
#include "../structure/Structure.hpp"

/**
 * Cette classe est une case elementaire du plateau
 */
class Cellule {

public:
    Cellule(EvenementPtr _evenement = 0, TypeCellule _type = TypeCellule::Vide);
    
    bool possedeEmplacement(TypeCellule _type) const;
    void setType(TypeCellule _type);
    TypeCellule getType() const;
    TypeCellule statutEmplacement() const;
    
    void subir(Structure const& attaquant);
    Structure getAttaquant();
    
    bool possedeEvenement() const;
    EvenementPtr getEvenement() const;
    
    bool possedeBatiment() const;
    TypeBatiment typeBatiment() const;
    BatimentPtr getBatiment() const;
    
    bool possedeVaisseau() const;
    void retirerVaisseau();
    VaisseauPtr getVaisseau() const;
    void setVaisseau(VaisseauPtr _vaisseau);
    
    int distanceMaximale() const;
    bool estAccessible() const;
    int getCoutDeplacement() const;

    //Multiple fonctions de test
    void creerVaisseauTest();
    void creerVaisseauConstructeurTest();
    void creerBatimentBaseTest();

private:
    EvenementPtr evenement;
    BatimentPtr batiment;
    VaisseauPtr vaisseau;
    TypeCellule type;

};

sf::Packet& operator <<(sf::Packet& paquet, const Cellule& cellule);

#endif // CELLULE_HPP
